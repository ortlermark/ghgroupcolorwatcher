﻿using Grasshopper;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Special;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace GroupColorWatcher
{
    //Big Thanks to David Rutten on:
    //http://www.grasshopper3d.com/forum/topics/groupcolorwatcher-component

    public abstract class ghGroupColorWatcherBase_Comp : GH_Component
    {
        private readonly Color DefaultColor = Color.Black;

        protected ghGroupColorWatcherBase_Comp(string name, string nickname, string description, string category, string subCategory)
          : base(name, nickname, description, category, subCategory)
        {
            GroupColor = DefaultColor;
        }

        public Color GroupColor { get; private set; }

        /// <summary>
        /// return true to search for a enclosing Groups color.
        /// </summary>
        protected virtual bool SearchForColor()
        {
            return true;
        }

        /// <summary>
        /// Called if the GroupColor was changed.
        /// Base-Implementation is empty.
        /// </summary>
        /// <param name="document"></param>
        protected virtual void GroupColorChanged(GH_Document document) { }

        /// <summary>
        /// Find the first group (if any) which contains this object.
        /// </summary>
        /// <returns>Group containing this object or null.</returns>
        public GH_Group FindGroup()
        {
            return FindGroup(OnPingDocument());
        }

        public GH_Group FindGroup(GH_Document doc)
        {
            if (doc == null)
                return null;

            if (SearchForColor())
            {
                foreach (IGH_DocumentObject obj in doc.Objects)
                {
                    GH_Group group = obj as GH_Group;
                    if (group == null)
                        continue;

                    if (group.ObjectIDs.Contains(InstanceGuid))
                    {
                        if (!group
                                .Objects()
                                .Where(x => (x as GH_Group) != null)
                                .Any(g => (g as GH_Group).ObjectIDs.Contains(InstanceGuid))
                            )
                        {
                            return group;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Find the colour as specified by the grouping state.
        /// </summary>
        public Color FindColour()
        {
            GH_Group group = FindGroup();

            if (group == null)
                return DefaultColor;

            return group.Colour;
        }
        /// <summary>
        /// Find the colour as specified by the grouping state.
        /// </summary>
        public Color FindColour(GH_Document doc)
        {
            GH_Group group = FindGroup(doc);
            if (group == null)
                return DefaultColor;

            return group.Colour;
        }
        /// <summary>
        /// Canvas paint event handler. 
        /// </summary>
        private void CanvasPaint(GH_Canvas sender)
        {
            if (!ReferenceEquals(sender.Document, OnPingDocument()))
                return;

            Color color = FindColour(sender.Document);
            if (GroupColor != color)
            {
                GroupColor = color;
                GroupColorChanged(sender.Document);
            }
        }

        public override void AddedToDocument(GH_Document document)
        {
            base.AddedToDocument(document);

            GH_Canvas canvas = Instances.ActiveCanvas;
            if (canvas != null)
            {
                canvas.CanvasPaintBegin -= CanvasPaint;
                canvas.CanvasPaintBegin += CanvasPaint;
            }
        }

        public override void RemovedFromDocument(GH_Document document)
        {
            base.RemovedFromDocument(document);

            GH_Canvas canvas = Instances.ActiveCanvas;

            if (canvas != null)
                canvas.CanvasPaintBegin -= CanvasPaint;
        }
    }
}
