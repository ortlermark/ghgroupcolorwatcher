﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace GroupColorWatcher
{
    public class ghGroupColorWatcherInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "ghGroupColorWatcher";
            }
        }
        public override Bitmap Icon
        {
            get { return Properties.Resources.Icons_Get; }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "A component to evaluate/change the color of the enclosing group.";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("fb77ca0c-2b71-4c9f-8053-c5465b590d03");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "Ortler Mark";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "ortlermark@gmail.com";
            }
        }
    }
}
