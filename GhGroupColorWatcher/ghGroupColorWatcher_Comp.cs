﻿using System;
using System.Drawing;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Special;

namespace GroupColorWatcher
{
    public class ghGroupColorWatcher : ghGroupColorWatcherBase_Comp
    {
        public ghGroupColorWatcher()
            : base("GroupColor", "GC", "A component to evaluate the color of the enclosing group.", "Extra", "GroupColorWatcher")
        {
            
        }

        protected override void RegisterInputParams(GH_InputParamManager pManager) { }
        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddColourParameter("GroupColor", "GroupColor", "", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess da)
        {
            da.SetData(0, GroupColor);
        }

        protected override Bitmap Icon
        {
            get { return Properties.Resources.Icons_Get; }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{d8afc75b-d201-4246-8944-6327a9ee70ef}"); }
        }
 

        protected override void GroupColorChanged(GH_Document document)
        {
            document.ScheduleSolution(10, Expire);
        }

        private void Expire(GH_Document document)
        {
            ExpireSolution(false);
        }
    }
}