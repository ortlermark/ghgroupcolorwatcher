﻿using System;
using System.Drawing;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Special;

namespace GroupColorWatcher
{
    //Big Thanks to David Rutten on:
    //http://www.grasshopper3d.com/forum/topics/change-component-group-color-on-the-fly

    public class ghGroupColorSwitcher_Comp : GH_Component
    {
        public ghGroupColorSwitcher_Comp()
            : base("GroupColorSwitcher", "GCS", "", "Extra", "GroupColorWatcher")
        {
            
        }

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("name", "name", "name of the groups to change", GH_ParamAccess.item);
            pManager.AddColourParameter("color", "color", "color to give all proper named groups", GH_ParamAccess.item);
        }
        protected override void RegisterOutputParams(GH_OutputParamManager pManager) {}
        protected override void SolveInstance(IGH_DataAccess da)
        {
            string gn = "";
            Color col = Color.Empty;
            da.GetData(0, ref gn);
            da.GetData(1, ref col);

            ColorUpGroup(OnPingDocument(), gn, col);
        }

        public void ColorUpGroup(GH_Document doc, string group2Color, Color color)
        {
            if (doc == null)
                return;

            foreach (IGH_DocumentObject obj in doc.Objects)
            {
                GH_Group group = obj as GH_Group;
                if (group != null)
                    if (group.NickName == group2Color)
                        group.Colour = color;
            }

            return;
        }

        protected override Bitmap Icon
        {
            get { return Properties.Resources.Icons_Set; }
        }
        public override Guid ComponentGuid
        {
            get { return new Guid("{9DAC55CA-CE88-4DD8-BF6E-B8240044E0C0}"); }
        }
    }
}